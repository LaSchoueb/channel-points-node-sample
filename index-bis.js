const got = require("got");
require("dotenv").config();

const customRewardBody = {
  title: "Changer de scène",
  prompt: "Change de scène pour un scène random ",
  cost: 1,
  is_enabled: true,
  is_global_cooldown_enabled: true,
  global_cooldown_seconds: 1,
};

let ArrayDataFromAPI = [
  [
    {
      token: "ou9w39smnazlej99eewi39zor7mqfp",
      clientId: "",
      userId: "",
      headers: "",
      validToken: ""
    },
    {
      title: "Changer de scène",
      prompt: "Change de scène pour un scène random ",
      cost: 1,
      rewardId: "",
      is_enabled: true,
      is_global_cooldown_enabled: true,
      global_cooldown_seconds: 1,
    },
    {
      title: "Changer de scène",
      prompt: "Change de scène pour un scène random ",
      cost: 1,
      rewardId: "",
      is_enabled: true,
      is_global_cooldown_enabled: true,
      global_cooldown_seconds: 1,
    },
  ],
  [
    {
      token: "ou9w39smnazlej99eewi39zor7mqfp",
      clientId: "",
      userId: "",
      headers: "",
      rewardId: "",
      validToken : ""
    },
    {
      title: "Changer de scène",
      prompt: "Change de scène pour un scène random ",
      cost: 2,
      rewardId: "",
      is_enabled: true,
      is_global_cooldown_enabled: true,
      global_cooldown_seconds: 1,
    },
  ],
];

let pollingInterval;

async function validateToken(streamerToken, e) {
  let r;
  try {
    let { body } = await got(`https://id.twitch.tv/oauth2/validate`, {
      headers: {
        Authorization: ` Bearer ` + streamerToken,
      },
    });
    r = JSON.parse(body);
  } catch (error) {
    console.log(
      'Invalid streamerToken. Please get a new token using twitch token -u -s "channel:manage:redemptions user:edit:follows"'
    );
    return false;
  }

  if (
    r.scopes.indexOf("channel:manage:redemptions") == -1 ||
    r.scopes.indexOf("user:edit:follows") == -1 ||
    !r.hasOwnProperty("user_id")
  ) {
    console.log(
      'Invalid scopes. Please get a new token using twitch token -u -s "channel:manage:redemptions user:edit:follows"'
    );
    return false;
  }

  // update the global variables to returned values
  e.clientId = r.client_id;
  e.userId = r.user_id;
  e.headers = {
    Authorization: ` Bearer ` + streamerToken,
    "Client-ID": e.clientId,
    "Content-Type": "application/json",
  };

  return true;
}

async function getCustomRewards (e) {
  try {
    let { body } = await got(
      `https://api.twitch.tv/helix/channel_points/custom_rewards?broadcaster_id=${e.userId}`,
      { headers: e.headers }
    );
    return JSON.parse(body).data;
  } catch (error) {
    console.log(error);
    return null;
  }
};

async function addCustomReward (newReward,e) {
  try {
    let { body } = await got.post(
      `https://api.twitch.tv/helix/channel_points/custom_rewards?broadcaster_id=${e.userId}`,
      {
        headers: headers,
        body: JSON.stringify(newReward),
        responseType: "json",
      }
    );

    newReward.rewardId = body.data[0].id;
    return true;
  } catch (error) {
    console.log("Failed to add the reward. Please try again.");
    return false;
  }
};


async function pollForRedemptions (userId,rewardId)  {
  try {
    
    let { body } = await got(
      `https://api.twitch.tv/helix/channel_points/custom_rewards/redemptions?broadcaster_id=${userId}&reward_id=${rewardId}&status=UNFULFILLED`,
      {
        headers: headers,
        responseType: "json",
      }
    );

    let redemptions = body.data;
    let successfulRedemptions = [];
    let failedRedemptions = [];

    for (let redemption of redemptions) {
      // can't follow yourself :)
      if (redemption.broadcaster_id == redemption.user_id) {
        failedRedemptions.push(redemption.id);
        continue;
      }
      // if failed, add to the failed redemptions
      if (
        (await followUser(redemption.broadcaster_id, redemption.user_id)) ==
        false
      ) {
        failedRedemptions.push(redemption.id);
        continue;
      }
      // otherwise, add to the successful redemption list
      successfulRedemptions.push(redemption.id);
    }

    // do this in parallel
    await Promise.all([
      fulfillRewards(successfulRedemptions, "FULFILLED"),
      fulfillRewards(failedRedemptions, "CANCELED"),
    ]);

    console.log(
      `Processed ${
        successfulRedemptions.length + failedRedemptions.length
      } redemptions.`
    );

    // instead of an interval, we wait 15 seconds between completion and the next call
    pollingInterval = setTimeout(pollForRedemptions(userId,rewardId), 15 * 1000);
  } catch (error) {
    console.log("Unable to fetch redemptions.");
  }
};

async function followUser (fromUser, toUser) {
  try {
    await got.post(
      `https://api.twitch.tv/helix/users/follows?from_id=${fromUser}&to_id=${toUser}`,
      { headers: headers }
    );
    return true;
  } catch (error) {
    console.log(`Unable to follow user ${toUser}`);
    return false;
  }
};

async function fulfillRewards (ids, status) {
  // if empty, just cancel
  if (ids.length == 0) {
    return;
  }

  // transforms the list of ids to ids=id for the API call
  ids = ids.map((v) => `id=${v}`);

  try {
    await got.patch(
      `https://api.twitch.tv/helix/channel_points/custom_rewards/redemptions?broadcaster_id=${userId}&reward_id=${rewardId}&${ids.join(
        "&"
      )}`,
      {
        headers,
        json: {
          status: status,
        },
      }
    );
  } catch (error) {
    console.log(error);
  }
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function main() {
  let validTokens = []; // initialisation d'un tableau de Token valide 
  let invalidTokens = []; // initialisation d'un tableau de Token invalide 
  let finalResult = [];
  for (let i = 0; i < ArrayDataFromAPI.length; i++) {
    if ((await validateToken(ArrayDataFromAPI[i][0].token, ArrayDataFromAPI[i][0])) == true) {// on parcours le tableau de tokens trnasmis par tructruc pour relaiser les verification et trier les tokens 
      validTokens[validTokens.length] = e;
      ArrayDataFromAPI[i][0].validToken = true;
    } else {
      invalidTokens[invalidTokens.length] = e[0].token;
      ArrayDataFromAPI[i][0].validToken = false;
    }
  }

  for (let i = 0; i < ArrayDataFromAPI.length; i++) {
      const streamerToken = ArrayDataFromAPI[i][0].token;
      if (ArrayDataFromAPI[i][0].validToken) 
        {
          for (let j = 1; j < ArrayDataFromAPI[i].length; j++) 
            {
              let rewards = await getCustomRewards(streamerToken);// récupération des rewards bits avec la key Twitch 
              console.log(rewards)
              if (rewards != null) 
                {
                  for (let k = 0; k <= rewards.length - 1; k++) 
                    {
                      const reward = rewards[k];
                      if ( typeof rewards[k] != 'undefined' )
                        { 
                          if (reward.title == eventArray.title) {
                            ArrayDataFromAPI[i][j].rewardId = reward.id;
                          }
                        }
                    }
                } else {
                console.log(
                  "The streamer does not have access to Channel Points. They need to be a Twitch Affiliate or Partner."
                );
              }
              /*if (eventArray.rewardId == "" && (await addCustomReward(eventArray,e)) == false) {
                return;
              }*/
          //finalResult[i] = eventArray
          
            }
        }
    /*finalResult.forEach(final => {
      pollForRedemptions(final.userId,final.rewardId);
    });*/
    
  }
  //console.log(ArrayDataFromAPI);
}

main();
